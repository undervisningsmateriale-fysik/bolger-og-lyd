\chapter{Resonans}
\label{chap:resonans}
\backgroundsetup{
position=current page.north east,
vshift=-35pt,
hshift=-200pt,
angle=0,
scale=0.4,	
opacity=0.2,
contents={\includegraphics{./grafik/forside/my-by-sa.png}}
}
\BgThispage


\begin{figure*}[h]
\includegraphics[width=\linewidth]{./grafik/bolger/tacoma2.png}
\caption{Tacoma bridge øjeblikke før den styrter sammen. \citep{tacoma}}
\label{fig:tacoma}
\end{figure*}

\newthought{Resonans} er et meget vigtigt begreb i fysik. Resonansfænomener opstår i mange vidt forskellige fysiske systemer, lige fra broer og musikinstrumenter til lasere, atomer og molekyler.   

\begin{marginfigure}[4cm]
\centering
\includegraphics[width=\linewidth]{./grafik/bolger/matter-structure.png}
\caption{Atomerne i stof er bundet sammen af kræfter der opfører sig som små fjedre. Det betyder at atomerne hele tiden svinger en lille smule omkring deres ligevægtsposition.}
\label{fig:matter-structure-margin}
\end{marginfigure}

Alt stof er opbygget af atomer som er bundet til hinanden, og det viser sig at denne binding mellem atomerne opfører sig lidt som en fjeder. En simpel model for stoffets opbygning ses på figur \vref{fig:matter-structure-margin}. Fordi atomerne er bundet sammen af kræfter der virker lidt som små fjedre, så kan alt stof bringes i svingninger. Men hvert stof har nogle helt bestemte frekvenser som det helst vil svinge med\footnote{det afhænger, groft sagt, af hvor kraftigt atomerne er bundet sammen, dvs. hvor kraftige fjedrene er}. Sådanne frekvenser kaldes for resonansfrekvenser\index{resonansfrekvens}\footnote{Naturlig frekvens og egenfrekvens er to andre ord som anvendes for det samme begreb.}, og hvis man påvirker (skubber til) et system med systemets resonansfrekvens kan man pumpe energi ind i systemet og derved opnå resonans\index{resonans}. Nedenfor er der givet forskellige eksempler på hvad det vil sige at opnå resonans.  
 


\section{Eksempler på resonansfænomener}

\subsection{En gynge}
En gynge  er et simpelt eksempel på et fysisk system hvori der kan opstå resonans. Gyngen har en bestemt længde, og tiden det tager for gyngen at svinge frem og tilbage afhænger kun af denne længde\footnote{Svingningstiden kan beregnes ud fra længden vha. følgende formel: \(T = 2\pi \sqrt{\frac{L}{g}} \quad \text{hvor} \quad g=9.82 \si{m/s^{2}}\)}.

Lad os sige at gyngen på figur \vref{fig:gynge-margin} har en svingningstid på 2 sekunder. Hvis man skal få gyngen til at svinge kraftigere skal man skubbe til gyngen på et helt bestemt tidspunkt. Man skal nemlig skubbe lige i det øjeblik gyngen skal til at bevæge sig fremad, skubber man for tidligt vil man blot bremse gyngen. Når gyngen vender tilbage efter yderligere 2 sekunder kan man give et nyt skub så gyngen svinger endnu højere. Man skal altså skubbe med en helt bestemt frekvens givet ved \( f = \frac{1}{T} \) for at få gyngen til at svinge så vildt som muligt. Dette er gyngens resonansfrekvens, dvs. netop den frekvens som gyngen helst vil svinge med.  

\begin{marginfigure}
\centering
\includegraphics[width=\linewidth]{./grafik/bolger/gynge.png}
\caption{Svingningstiden for en gynge afhænger kun af gyngens længde (\(L\)). Gyngens resonansfrekvens er givet ved \(f=\frac{1}{T}\). \citep{gynge}}
\label{fig:gynge-margin}
\end{marginfigure}



\subsection{En fjeder}
Et andet illustrativt eksempel på et resonansfænomen er en fjerder med et lod i den ene ende. Man kan få loddet til at svinge op og ned ved at bevæge fjederens modsatte ende op og ned. Fjederen har en helt bestemt resonansfrekvens og hvis man svinger med denne frekvens kommer loddet hurtigt til at svinge meget vildt op og ned. Hvis man derimod svinger med en frekvens der er større end fjederens resonansfrekvens, så kan man svinge op og ned med en meget stor amplitude uden at loddet bevæger sig særlig meget. 


\subsection{Et vinglas}
Hvis man slår på et vinglas kan man høre at glasset afgiver en bestemt tone. Det skyldes at atomerne i glasset svinger med en bestemt frekvens, nemlig glasset resonansfrekvens. Hvis man kan synge denne tone med tilstrækkelig stor amplitude kan man faktisk ødelægge glasset fordi atomerne kommer til at svinge så kraftigt at glasset går i stykker. En meget illustrativ video det viser dette fænomen \href{https://www.youtube.com/watch?v=BE827gwnnk4}{kan ses her}. \marginnote{\url{https://www.youtube.com/watch?v=BE827gwnnk4}}    


\subsection{En stemmegaffel}
Når man slår på en stemmegaffel afgiver den ligesom vinglasset en helt bestemt tone der svarer til stemmegaflens resonansfrekvens. Det sker fordi stemmegaflens to ben begynder at svinge frem og tilbage. Disse svingninger har en meget lille amplitude og kan derfor ikke ses med dette blotte øje. Desuden er svingningen også meget hurtig. En A-tone har frekvensen 440 Hz, dvs. benene svinger frem og tilbage 440 gange hvert sekund! Dette er alt for hurtigt til at vores øjne kan opfatte det. Hvis man prøver at svinge sin hånd op og ned foran øjnene så vil man opdage at man ikke skal svinge særlig hurtigt før hånden flyder ud og det ser ud som om at den er flere steder samtidigt. 


\begin{marginfigure}[-2cm]
\centering
\includegraphics[width=\linewidth]{./grafik/bolger/stemmegaffel.png}
\caption{Når stemmegaflen slås an vil benene begynde at svinge frem og tilbage med stemmegaflens resonansfrekvens. Denne figur viser hvordan benene svinger i løbet af en periode. }
\label{fig:stemmegaffel-margin}
\end{marginfigure}





\section{Stående bølger}
\label{sec:staaende-bolger}
Vi skal nu se på et vigtigt eksempel på et resonansfænomen, det man i fysik kalder for en stående bølge.

Hvis en streng bringes til at svinge med én af sine resonansfrekvenser, fremkommer det fænomen som kaldes for en stående bølge. Det ser ud som om man har en bølge der svinger op og ned på tværs af strengen uden at bevæge sig hverken frem eller tilbage - deraf navnet stående bølge. 

\begin{marginfigure}
\centering
\includegraphics[width=\linewidth]{./grafik/bolger/StandingWaves.png}
\caption{Stående bølger som opstår ved hhv. den laveste, næstlaveste, og tredjelaveste resonansfrekvens for en streng.}
\label{fig:standing-waves-margin}
\end{marginfigure}

En stående bølge kan dannes som vist på figur \vref{fig:melde} ved at have en vibrator i den ene ende som med en meget lille amplitude svinger strengen op og ned. Dette sender en tværbølge afsted langs strengen, og når den indkommende bølge rammer enden hvor strengen er fastgjort bliver bølgen reflekteret og bevæger sig tilbage mod vibratoren. Når den reflekterede bølge interfererer med den indkommende bølge opstår den stående bølge. En simulering af hvordan en stående bølge opstår kan man fx ses på \href{http://tube.geogebra.org/student/m67208}{denne Geogebra animation}. \marginnote{\url{http://tube.geogebra.org/student/m67208}} 
 
\begin{figure}
\centering
\includegraphics[width=\linewidth]{./grafik/bolger/melde.png}
\setfloatalignment{b}
\caption{Eksperiment til undersøgelse af stående bølger på en streng.}
\label{fig:melde}
\end{figure}


Figur \vref{fig:staaendebolge} forsøger også at illustrere hvordan en stående bølge kan ses som en sum af to bølger der bevæger sig mod hinanden. Det er illustreret hvordan den stående bølge ændrer sig over tid. Bemærk at nogle punkter, de såkaldte knudepunkter, aldrig bevæger sig, og det maksimale udsving findes altid midt mellem to knudepunkter.   


\begin{figure*}[htpb]
\centering
\def\tkzscl{1}
\input{./grafik/bolger/tikz/staaendebolge.tikz}
%\setfloatalignment{b}
\caption{En stående bølge ændrer sig som tiden går. Der forløber tiden \(T/2\) fra øverste til nederste figur. Den stående bølge (sort) består af summen af en blå bølge som bevæger sig mod højre og en rød bølge der bevæger sig mod ventre. I knudepunkterne (magenta) er der altid destruktiv interferens, mens det maksimale udsving findes midt mellem knudepunkterne.}
\label{fig:staaendebolge}
\end{figure*}



Efter lidt overvejelse kan det indses, at der skal være en sammenhæng mellem strengens længde og bølgens bølgelængde for at der kan opstå en stående bølge (resonans). Når man skubber en person på en gynge skal man skubbe lige på det rette tidspunkt for at give personen mere fart på. På samme måde skal den reflekterede bølge komme tilbage til vibratoren lige præcis på det tidspunkt hvor den kan få et skub mere. Hvis timingen passer, vil vibratoren pumpe mere og mere energi ind i bølgen for hver omgang. Hvis timingen derimod ikke passer vil bølgen ikke opbygge en stor amplitude og vi vi ikke se en stående bølge men blot en svinging med en meget lille amplitude. 

Ved yderligere (matematisk) overvejelse kan man indse at der skal være en sammenhæng som angivet i ligning \eqref{eq:L-og-lambda} mellem strengens længde og bølgens bølgelængde for at der kan opstå en stående bølge. 

\marginnote{ 
\begin{tikzpicture}
 \draw [beaublue, line width = 4] (0,0) -- (0.25,0); 
\end{tikzpicture}
\opgref{opg:formel-for-L-og-lambda}
}


\begin{formelboks}
Stående bølger på en streng kan opstå når strengens længde svarer til et helt antal halve bølgelængder. 

\begin{equation} \label{eq:L-og-lambda}
L = n \cdot \frac{\lambda}{2} \quad  \text{hvor} \quad n=1,2,3, \ldots
\end{equation}


L er strengens længde. \( \lambda \) er bølgelængden og \(n\) er et vilkårligt positivt helt tal. Når \(n=1\) fås den første stående bølge, når \(n=2\) fås den næste stående bølge osv. 
\end{formelboks}


I afsnittet om lyd og musik skal vi bruge denne viden om stående bølger til at forstå hvordan man sørger for at instrumenter kan spille de ønskede toner, og hvorfor to instrumenter har forskellig klang selvom de spiller den samme tone. 

Når vi senere skal beskæftige os med atomer, kan vi, måske lidt overraskende, også trække på vores viden om stående bølger.
Da Niels Bohr og andre af kvantefysikkens pionerer forsøgte at opstille en model for atomet brugte de deres viden om den mere håndgribelige situation med stående bølger på en streng til at forestille sig hvordan en elektron kunne ses som en 3 dimensionel stående bølge der svinger omkring atomets kerne. Når man skal udvikle modeller for utilgægelige mikroskopiske fænomener som fx atomer og molekylers opbygning, så er det meget almindeligt at man trækker på sin viden om mere håndgribelige makroskopiske fænomener som man direkte kan se og føle på. 
