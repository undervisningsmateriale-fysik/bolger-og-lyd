% 1 draft: msm
% 1 revision: 
% 2 revision: 

\chapter{Lys som bølger}
\label{chap:lys-bolger}

\begin{figure*}[h]
\includegraphics[width=\linewidth]{./grafik/bolger/symphony-of-lights-cropped.png}
\caption{Hong Kong Symphony of Lights laser show. \citep{symphony-lights}}
\label{fig:symphony-of-lights}
\end{figure*}

\section{Bølger eller partikler?}

Christiaan Huygens (1629-1695) var en hollandsk fysiker, astronom og matematiker. Huygens var overbevist om at lys skulle beskrives som bølger. Han skriver bogen >>Om Lyset<< hvori han blandt meget andet gør rede for, hvordan mange kendte lysfænomener kan forklares, når man opfatter lyset som bølger. Huygens kan bl.a. forklare hvorfor lyset udbreder sig langs rette linjer, at lyset skifter retning når det bevæger sig fra ét gennemsigtigt medium til et andet. Huygens kan også udlede brydningsloven og spejlingsloven som er beskrevet i afsnit \vref{sec:brydning}. Huygens beskrivelse af lys som en bølge, kaldes i dag for lysets bølgemodel. Som denne sprogbrug nok afslører, så eksisterer der også andre modeller for lys, heriblandt en partikelmodel som oprindeligt blev introduceret Af Isaac Newton. 


Trods Huygens opdagelser var fysikerne stadige uenige om hvad lys >>i virkeligheden var<<, skulle lys forstås som en bølge, eller som en strøm af små partikler. Årsagen til denne uenighed var, at Newton havde lavet en partikelmodel for bølger som også kunne forklare mange af de samme fænomener, som Huygens kunne forklare med sin bølgemodel.  
To tilsyneladende forskellige modeller kan altså forklare de samme fænomener. Hvordan i alverden finder man så ud af hvilke model der er korrekt, for de kan da ikke begge være korrekte, vel?

\begin{marginfigure}
\centering
\includegraphics[width=\linewidth]{./grafik/bolger/christiaan-huygens-painting.jpeg}
\caption{Christian Huygens (1629-1695).}
\label{marginfig:gitter}
\end{marginfigure}

Hvis to teorier altid har de samme observerbare konsekvenser, så må de fra en fysikers synspunkt betragtes som ens. Men rivaliserende modeller kan sagtens være enige om en lang række ting, uden at være enige om alt. Fysikerens opgave er så at designe eksperimenter der kan teste modellerne der hvor de er uenige, og på den måde afgøre hvilken model der er >>korrekt<<. 

Først da Thomas Young i starten af 1800-tallet lavede det såkaldte dobbeltspalte eksperiment som viste at lys bliver afbøjet i et gitter på samme måde som andre bølger, tippede vægtskålen afgørende til fordel for bølgemodellen. Bølgemodellen forblev fysikernes foretrukne model for lys i næsten 100 år indtil nye eksperimenter af den såkaldte fotoelektriske effekt viste, at man nogen gange alligevel er nødt til at opfatte lys som partikler. Dvs. nogle eksperimenter giver evidens for at lys er en slags bølge mens andre eksperimenter giver evidens for at lys er en strøm af små partikler.
Men lys kan da ikke både være en bølge og en partikel, kan det? I en vis forstand kan det faktisk godt, og man kalder dette fænomen for lysets bølge/partikel dualitet.

Disse erfaringer har lært os at vi i stedet for at sige, at en model er korrekt (eller forkert) egentlig bør sige at modellen er sandsynlig (eller usandsynlig) på baggrund af de hidtil udførte eksperimenter.
Og det er vigtigt at bemærke, at selvom en model ikke er korrekt i alle henseender, kan den stadig være utrolig brugbar indenfor det domæne hvor modellen gælder.
Newtons love gælder fx ikke ved meget høje hastigheder, i dette domæne er modellen strengt taget forkert og skal erstattes af Einsteins relativitetsteori. Men det betyder ikke at Newtons love ikke er brugbare, modellen er stadig så god at den kan bruges til sende mennesker til månen og tilbage igen.  
Man kan altså ikke bare sige om en model, at den er korrekt eller forkert, der er mange gradbøjninger af korrekt og forkert. 



\section{Gitterligningen}
Vi ved at lys i mange situationer kan opfattes som bølger. Bølgelængden er en af de mest grundlæggende bølgeegenskaber, men hvordan måler man egentlig bølgelængden af lys? Lys bevæger sig med en hastighed på \( 300000 \si{km/t} \), og man kan ikke bare tage et billede af en lysbølge og måle fra bølgetop til bølgetop, som vi kan med andre bølger. Vi er nødt til at finde en anden måde at bestemme lysets bølgelængde på.

Når man skal bestemme bølgelængden af lys udnytter man, hvad man ved om andre typer af bølger. Det er let at betragte ved eksperimenter, at en vandbølge opfører sig som vist på figur \vref{fig:vand-dobbeltspalte} når bølgerne møder en forhindring som fx et gitter.

\begin{figure}
\centering
\includegraphics[width=\linewidth]{./grafik/bolger/doubleslit.png}
\caption{En plan bølge kolliderer med et gitter. På bagsiden fremkommer et interferensmønster. \citep{gitter-interferens}}
\label{fig:vand-dobbeltspalte}
\end{figure}

Fra hvert hul i gitteret dannes en (ring)bølge. Når disse bølger interfererer, vil der i nogle retninger være konstruktiv interferens, og i andre retninger vil der være destruktiv interferens. Denne opførsel kan man sammenfatte i en formel som man kalder gitterligningen\index{gitterligningen}  

\begin{marginfigure}
\centering
\includegraphics[width=\linewidth]{./grafik/bolger/ripples-waves-bee.jpg}
\caption{En uheldig bi, der er landet i en sø, svirrer med vingerne og giver anledning til to bølgekilder som interfererer. \citep{bee-wave-interference}}
\label{marginfig:bee-wave-interference}
\end{marginfigure}


\begin{formelboks}
Afbøjning af en plan bølge i et gitter.

\begin{equation}
\sin{(\theta_{n})} \cdot d = n \cdot \lambda \quad \quad \text{Hvor} \quad n=0,1,2,3,\ldots
\label{eq:gitter-ligning}
\end{equation} 

\( \theta_{n} \) er afbøjningsvinklen af den n'te orden. \(d\) er gitterkonstanten. \( \lambda \) er bølgens bølgelængde og \(n\) er ordensnummeret.   
\end{formelboks}

Hvis vi følger Huygens, er vores hypotese er at lys er en bølge og derfor kan vi jo håbe på at formlen også vil gælde for lys. Hvis dette er tilfældet så har vi en formel der kan bruges til at bestemme lysets bølgelængde, fordi alle andre størrelser som indgår i formlen, udover \( \lambda \), kan måles meget let.  


\subsection{Det optiske gitter}
Et optisk gitter \index{optisk gitter} er blot et gitter med en meget lille gitterkonstant, sådan at gitteret kan bruges til at afbøje lysbølger der har en meget lille bølgelængde. Fordi gitteret kan afbøje lys kaldes gitteret for optisk. Et optisk gitter er i sin simpleste form en glasplade, hvori der er graveret en række uigennemsigtige stave. Antallet af stave per millimeter bestemmer, hvordan gitteret påvirker det lys der rammer gitteret. Hvis gitteret har 100 stave per millimeter har hver stav bredden en hundrededel millimeter. Bredden af en stav kaldes gitterkonstanten.

\begin{marginfigure}
\centering
\includegraphics[width=0.6\linewidth]{./grafik/bolger/gitter.png}
\caption{Skitse af et optisk gitter med gitterkonstant \(d\).}
\label{fig:gitter}
\end{marginfigure}


\begin{figure}[htpb]
\centering
\includegraphics[width=\linewidth]{./grafik/bolger/optisk-gitter-red.png}
\setfloatalignment{b}
\caption{Spredning af rødt laserlys i et optisk gitter.}
\label{fig:optisk-gitter-red}
\end{figure}

Vi vil nu se på hvad der sker når ensfarvet (monokromatisk) lys rammer et gitter, situationen er illustreret på figur \vref{fig:optisk-gitter-red}. Det monokromatiske lys kan fx komme fra en HeNe laser. Før gitteret sættes ind i lysstrålen rammer lyset fra laseren vinkelret på skærmen. Lyspletten på skærmen kalder vi 0. ordens pletten. Gitteret sættes ind så lysstrålen rammer vinkelret på gitteret. Efter at gitteret er sat ind ses et mønster af pletter på skærmen. Mønsteret er symmetrisk om 0. ordens pletten, hvis lysstrålen, som krævet, er vinkelret på skærm og gitter. Pletten nærmest 0. orden kaldes for 1. ordens pletten og den næste kaldes 2. ordens pletten osv. 


Man kan også bruge et optisk gitter når man vil undersøge farvesammensætningen af lys, dette viser sig af være meget anvendeligt fx når man skal bestemme hvilke grundstoffer der findes på fjerne stjerner og planeter.
Tænker vi os nu at lyset kommer fra en lyskilde der udsender lys med netop to farver, fx rød og blå, så vil vi i hver orden se netop to pletter nemlig en rød og en blå.
Bruger vi en lyskilde der udsender hvidt lys vil vi se et regnbuelignende sammenhængende spektrum i hver orden. Sammenhængende (kontinuerte) spektre fås typisk fra glødende legemer, som fx solen eller glødetråden i en pære.

\begin{marginfigure}
\centering
%\includegraphics[width=\linewidth]{./grafik/bolger/optisk-gitter-regnbue.png}
\includegraphics[width=\linewidth]{./grafik/bolger/diffraction-grating-rainbow.png}
\caption{Hvidt lys bliver spredt i et optisk gitter.}
\label{marginfig:optisk-gitter-regnbue}
\end{marginfigure}
  
  
%\section{Huygens princip}
%Huygens bølgeprincip fortæller, hvordan man ud fra en bølgefront kan konstruere sig frem til, hvor bølgefronten er til et senere tidspunkt. Med Huygens princip kan man med andre ord beskrive, hvordan bølger udbredes.
%
%\begin{formelboks}
%Huygens princip siger at:
%
%Alle punkter på en bølgefront er udgangspunkt for ringbølger. Til enhver senere tid kan bølgefrontens nye position bestemmes ved at finde en fælles tangent til ringbølgerne.
%\end{formelboks}
%
%
%\subsection{Lysets spejling}
%dfdfdsf
%
%\subsection{Lysets brydning}
%Vi skal nu vise, hvordan ringbølger (Huygens princip) også kan bruges når vi skal forstå, hvorfor lysstråler skifter retning når de går fra f.eks. luft til glas. Vi vil med andre ord prøve ud fra en bølgemodel for lys at forklare, hvorfor lyse skifter retning når det passerer fra et medium til et andet. Når vi har forstået dette retningsskift og beskrevet det, er vi også i stand til at forstå, hvorfor glaslinser virker som de gør.
%
%
%Nedenfor vises på første billede, hvordan to bølgetoppe bevæger sig ind mod en glasoverflade. Bølgetoppene, også kaldet bølgefronter, står vinkelret på bevægelsesretningen. Vi tænker os at de to bølgetoppe bevæger sig i luft før de rammer glasoverfladen.




\section{Videnskabsteoretiske overvejelser}
%Som vi har set i afsnit \vref{sec:huygen} kan Huygens princip forklare lysets brydning, %spejling, spredning/afbøjning i gitter mm.  

Bølgemodellen for lys kan forklare hvordan lyset spredes i et optisk gitter. Men kan man så deraf slutte at lys er bølger? 

Fysikeren Christiaan Huygens stillede sig selv det samme spørgsmål tilbage i starten af 1800-tallet. I sin bog >>Om Lyset<< havde han nemlig, ud fra en hypotese om at lys er bølger, kunne forklare mange lysfænomener, heriblandt spejlingsloven og brydningsloven. Lad os se hvad Huygens skriver om denne form for >>Bevis<<. Nedenfor følger et lille citat fra forordet i hans bog.  

%Ligeledes vises det i arbejdsarket \vref{chap:arbejdsark-gitterligning} at lysets spredning i et gitter kan forklares hvis man antager at lys er en bølge. 

\begin{quotation}
Der er i denne bog beviser af den slags, som ikke giver så stor vished, som beviserne i geometrien\footnote{På Huygens tid satte man lighedstegn mellem geometri og matematik}, og som også vil adskille sig meget fra dem. Thi eftersom de, som dyrker geometri, beviser deres sætninger ved hjælp af ubestridelige principper, vil principperne her blive vist ved de konklusioner der kan drages.

Thi disse tings natur tillader ikke, at det gøres på andre måder. Det er altid derved muligt at opnå en grad af vished, som ofte ikke er meget mindre end et komplet bevis, nemlig når de ting, som er vist ud fra principperne som de er antaget, er i perfekt overensstemmelse med det, som et eksperiment viser. Dette gælder specielt hvis der er mange eksperimenter.

Endvidere også hvis man kan forudse nye hypoteser, som følger af de allerede antagne og disse også er i overensstemmelse med det forventede. Hvis alle disse beviser baseret på den opnåede vished viser sig at holde, så er det min overbevisning at de udgør et stærkt argument for succesen af mine undersøgelser.
\end{quotation}

Huygens overvejelser er helt i tråd med den metode til at opnå viden, som vi i dag kalder for den \textit{hypotetisk deduktive metode}\index{hypotetisk deduktiv metode}.