\chapter{Matematik og bølger}

\begin{figure*}
\centering
\includegraphics[width=\linewidth]{./grafik/bolger/stones-and-water-surface-wave-2.png}
\caption{Ringbølge i vandoverflade. \citep{surface-water-wave-and-stones}}
\label{fig:sinusbolge-i-vandoverflade}
\end{figure*}

\newthought{Matematik og bølger}. I dette afsnit skal vi se nærmere på den matematiske teori for bølger. Afsnittet kan fx bruges i et tværfagligt forløb om bølger i fysik og matematik. 

\section{Sinusfunktionen}

\subsection{Rumlig variation}
Hvis man ser et fotografi af en bølge der udbreder sig i en vandoverflade, så får man hurtigt den ide at vandoverfladens form måske kan beskrives af en sinusfunktion. 

\begin{formelboks}
En sinusfunktion med amplitude A, og en periode på \(2\pi\) har følgende forskrift:

\begin{equation}
 f(x) = A \cdot \sin{(x)}
\end{equation}

\( f(x) \) angiver bølgens udsving ved en bestemt position \(x\).
\end{formelboks}

\begin{figure}
\centering
\includegraphics[width=\linewidth]{./grafik/bolger/sinusbolge-position.png}
\caption{Figuren viser en sinusbølges udsving som funktion af position.}
\label{fig:sinusbolge-pos}
\end{figure}

Figur \vref{fig:sinusbolge-pos} viser grafen for en sådan sinusfunktion. 
Bemærk at bølgemønsteret gentager sig med en periode på \( 2 \pi \). Dvs. hver gang man bevæger sig \( 2 \pi \approx 6.28 \) frem langs x-aksen, så vil sinusfunktionen (dvs. \(f(x)\)) have samme værdi. Bølgemønsteret fra en sinusfunktion gentager altså sig selv med en periode på \( 2 \pi \). Det betyder at afstanden mellem to på hinanden følgende bølgetoppe eller bølgedale vil være \( 2\pi\). 	I matematiksprog kan man skrive denne erkendelse kort og præcist på følgende måde:


\[ A \cdot \sin{(x)} = A \cdot \sin{(x+ 2\pi)} \] 

Eller blot som,

\[ f(x) = f(x+2\pi) \]

hvor \( f(x) \) angiver bølgens udsving som funktion af \( x \). Den rumlige periode kalder vi i fysik for bølgelængden. I ovenstående tilfælde er bølgelængden altså lig med \(2 \pi\). Men det er nok de færreste bølger i naturen der har en bølgelængde på præcis \(2\pi\), så hvordan beskriver man en bølge med en anden bølgelængde end \( 2\pi\)?

\marginnote{ 
\begin{tikzpicture}
\draw [beaublue, line width = 4] (0,0) -- (0.25,0); 
\end{tikzpicture}
\opgref{opg:periode-lambda}
}

\begin{formelboks}
Sinusfunktion med en \textbf{rumlig} periode på \(\lambda\).

\begin{equation}
f(x) = A \cdot \sin{\left( \frac{2\pi}{\lambda} \cdot x \right)}
\end{equation}

Hver gang man går \( \lambda \) hen langs x-aksen, så vil \(f(x)\) have samme værdi. Den rumlige periode \( \lambda \) kalder vi i fysik for bølgelængden.
\end{formelboks}

Benyt \href{http://ggbtu.be/mAPawCCWi}{denne animation} \marginnote{\url{http://ggbtu.be/mAPawCCWi}} til at undersøge hvordan sinusfunktionen ændrer udseende når du ændrer på \(\lambda \) eller amplituden \( A \).



\subsection{Tidslig variation}
Hvis man i stedet for et fotografi af en bølge, ser en film af en bølge der udbreder sig i en vandoverflade. Så vil man se, at bølgen ændrer form når tiden går. Dvs. udover at bølgens udsving er forskellig på forskellige steder, så vil udsvinget alle stederne også ændre sig når tiden går.

Lad os nu se nærmere på hvordan bølgen ændrer sig i tid, dvs. bølgens tidslige variation. Vi forestiller os at vi kigger på en bølge et helt bestemt sted og ser hvordan bølgens udsving ændrer sig når tiden går. Man kan fx placere en bøje i vandoverfladen og observere hvordan højden af bøjen ændrer sig når der passerer en bølge. Det viser sig at hvis man måler udsvinget (bøjens højde) som funktion af tiden, så vil man få en pæn sinuskurve som vist i figur \vref{marginfig:sinusbolge-tid}. Denne sinuskurve ligner til forveksling sinuskurven i figur \vref{fig:sinusbolge-pos}. Bemærk dog at enheden langs x-aksen på figur \vref{marginfig:sinusbolge-tid} er sekunder, i modsætning til figur \vref{fig:sinusbolge-pos} hvor enheden langs x-aksen er meter. Tiden der går før bølgemønsteret gentager sig selv kaldes i fysik for perioden og betegnes med \( T \).  

\begin{marginfigure}
\centering
\includegraphics[width=\linewidth]{./grafik/bolger/sinusbolge-tid.png}
\caption{Figuren viser hvordan udsvinget et helt bestemt sted ændrer sig når tiden går.}
\label{marginfig:sinusbolge-tid}
\end{marginfigure}

Specielt i fysik taler man ofte om frekvensen af en bølge. Som beskrevet i afsnittet om bølgers egenskaber der starter på \vref{sec:bolgeegenskaber} fortæller frekvensen hvor mange svingninger der passerer, et bestemt sted, pr. sekund. Dvs. hvor mange gange i sekundet vores bøje bevæger sig og op ned. Som beskrevet nærmere i afsnittet om bølgers egenskaber kan frekvensen altså beregnes ud fra perioden på følgende måde: 

\[ f = \frac{1}{T} \]
  

Benyt \href{http://tube.geogebra.org/m/h5Bcp7g8}{denne animation} \marginnote{\url{http://tube.geogebra.org/m/h5Bcp7g8}} til at undersøge hvordan sinusfunktionen ændrer sig når du ændrer på \(\lambda \) eller \( T \). Animationen er lavet med henblik på at illustrere begreberne rumlig periode \(\lambda \) og tidslig periode \(T\), og på et punkt er animationen i uoverensstemmelse med virkelighedens verden. I virkelighedens verden kan man ikke variere \(T\) og \(\lambda \) uafhængigt af hinanden ligesom i animationen. Som du måske husker, så er \(\lambda\) og \(T\) nemlig forbundet via. bølgeformlen \eqref{eq:bolgeformlen} som siger at:
  
\[ v = \frac{\lambda}{T} \quad \text{Hvor \textit{v} er bølgens hastighed i materialet} \]

Og da bølgehastigheden i et bestemt materiale er en konstant, så vil \(\lambda\) og \(T\) faktisk være proportionale. 



\section{Bølger på en vandoverflade - bølger i 2 dimensioner}
En bølge på en vandoverflade som vist i figur \vref{fig:sinusbolge-i-vandoverflade}, er rent matematisk en forholdsvis kompliceret størrelse idet bølgen har forskellige udsving (værdier) forskellige steder i rummet, og på alle disse steder i rummet kan bølgen ændre sig når tiden går.


På \href{https://dl.dropboxusercontent.com/u/5481734/opentextbooks/animationer/surface-wave-with-point-ver1.avi}{dette link} \marginnote{\url{https://dl.dropboxusercontent.com/u/5481734/opentextbooks/animationer/surface-wave-with-point-ver1.avi}} ses en simulering af en overfladebølge der udbreder sig fra et punkt på vandoverfladen hvor der er blevet smidt en sten i vandet.


Et bestemt sted på vandoverfladen kan angives med koordinaterne \( (x,y) \), det røde punkt har koordinaterne \( (4,0) \). Når tiden går, bevæger punktet sig op og ned. Dvs. vandoverfladens udsving afhænger både af x-koordinaten, y-koordinaten og tiden \(t\).  


Udsvinget for en bølge har vi tidligere betegnet med \( f \). Rent matematisk siger vi at \( f \) er en funktion både af tiden \(t\) og positionen i rummet angivet ved kordinaterne \((x,y)\).
For at markere at bølgens udsving er en funktion af de tre variable \(x\), \(y\) og \(t\) skriver man \( f(x,y,t) \).


\section{Funktioner af flere variable}
\label{sec:funktioner-af-flere-variable}
Som i foregående afsnit, så skal man altså kende til det matematiske koncept om funktioner af flere variable, for at kunne beskrive bølger i den virkelige verden. I matematiktimerne har du sikkert beskæftiget dig med funktioner af 1 variabel, så lad os lige starte med at genopfrikse hvad vi ved om funktioner af 1 variabel. 

\begin{marginfigure}
\centering
\includegraphics[width=\linewidth]{./grafik/bolger/simple-function-of-1-variable.png}
\caption{En funktion af variablen \(x\).}
\label{marginfig:simple-function-1variable}
\end{marginfigure}

\begin{eks}
Her ses et simpelt eksempel på en funktion af 1 variabel.

\[ f(x) = 2 \cdot x + 3 \]  

funktionens navn er \(f\) og variablens navn er \(x\). For at angive at værdien af \(f\) afhænger af hvilket \(x\) man har valgt, så skriver man \(f(x)\). Funktionsforskriften ovenfor fortæller hvordan man beregner værdien af \(f\) for et givet \(x\). Hvis for eksempel \(x\) vælges til 1, så beregner man funktionen  \(f\) i punktet \(x=1\) på følgende måde:
 
\[ f(1) = 2 \cdot 1 + 3  = 5 \]

Og hvis man beregner f for en lang række af x-værdier så kan man indtegne grafen for funktionen i et almindeligt (2-dimensionalt) koordinatsystem. Figur \ref{marginfig:simple-function-1variable} viser grafen for \( f(x) = 2 \cdot x + 3 \).
\end{eks}




\subsection{En funktion af 2 variable}
Hvis en funktion \(f\) afhænger af 2 variable \(x\) og \(y\) skriver man \( f(x,y)\). Det betyder at man både skal vælge et \(x\) og et \(y\) for at kunne beregne \(f\).



\begin{eks}
Her ses et simpelt eksempel på en funktion af 2 variable \(x\) og \(y\).

\[ f(x,y) = 2 \cdot x + y \]  

Hvis for eksempel \(x\) vælges til 1, og \(y\) vælges til 5, så beregner man funktionen \(f\) i punktet \((1,5)\) (dvs. \(x=1\) og \(y=5\)) på følgende måde:

\[ f(1,5) = 2 \cdot 1 + 5 = 7 \]  

Og hvis man beregner \(f\) for en lang række af x- og y-værdier, så kan man indtegne grafen for funktionen i et 3 dimensionelt koordinatsystem. Figur \vref{fig:simple-function-2variables} viser grafen for \( f(x,y) = 2 \cdot x + y \).
\end{eks}

\begin{figure*}
\centering
\includegraphics[width=\linewidth]{./grafik/bolger/simple-function-of-2-variables.png}
\caption{En funktion af to variable \(x\) og \(y\).}
\label{fig:simple-function-2variables}
\end{figure*}

\subsection{En funktion af 3 variable}
Ligesom man kan have funktioner af 1 og 2 variable, kan man selvfølgelig også have funktioner af 3 (eller flere) variable.

\begin{eks}
Her ses et eksempel på en funktion af 3 variable \(x\), \(y\) og \(z\).

\[ f(x,y,z) = z \cdot x + y \]  

Hvis for eksempel \(x\) vælges til 1, \(y\) vælges til 5, og \(z\) vælges til 3 så beregner man funktionen \(f\) i punktet \((1,5,3)\) (dvs. \(x=1\), \(y=5\) og \(z=3\)) på følgende måde:

\[ f(1,5,3) = 3 \cdot 1 + 5 = 8 \]  


Funktioner af 1 eller 2 variable kunne vi visualisere ved at indtegne dem i et koordinatsystem. En funktion af 3 variable (eller flere) er derimod ikke så let at indtegne idet man ville have behov for et 4-dimensionelt koordinatsystem!
Da mennesker oplever at leve i et 3-dimensionelt rum, er der ingen der kan forestille sig hvordan et 4-dimensionelt rum ser ud, og derfor kan man heller ikke tegne \(f(x,y,z)\). 
\end{eks} 
 

\section{Bølger på en snor - bølger i 1 dimension} 
Når en bølge udbreder sig gennem et materiale (som fx en snor), så vil materialet bringes i svingninger. Udsvingets størrelse afhænger både af hvor man kigger og hvornår. Sagt mere præcist så afhænger bølgens udsving af 2 variable, nemlig positionen \( x \) og tiden \( t \). Dvs. rent matematisk kan snorbølgen modelleres af en funktion af 2 variable hvor den ene variabel er positionen \(x\) og den anden variabel er tiden \(t\). Åbn \href{https://phet.colorado.edu/sims/html/wave-on-a-string/latest/wave-on-a-string_en.html}{denne interaktive animation \citep{waves-on-string-animation}} \marginnote{\url{https://phet.colorado.edu/sims/html/wave-on-a-string/latest/wave-on-a-string_en.html}} for at se en bølge der bevæger sig langs en snor. 


\begin{formelboks}
Den simpleste matematiske funktion som kan beskrive en bølge på en snor er:

\begin{equation}
f(x, t) = A \cdot \sin{\left( \frac{2\pi}{\lambda}\cdot x - \frac{2\pi}{T}\cdot t\right)}
\end{equation}

Bemærk at bølgens udsving \(f(x,t)\) er en funktion af to variable, nemlig positionen \(x\) og tiden \(t\).
\end{formelboks}

Her vil udsvinget \(f(x,t)\) ændre sig som en sinusfunktion når enten \( x \) eller \( t \) ændres. Men bemærk at den rumlige periode \( \lambda \) og den tidslige periode \( T \) typisk har forskellige størrelser, og deres enheder er selvfølgelig også forskellige.




\section{Rejsende bølger}
Man kan skabe en bølge i en vandoverflade ved at smide en sten i vandet eller blot ved at prikke i vandoverfladen som vist på figur \vref{fig:water-surface-waves}. Hvis man gør det vil man se at bølgen bevæger sig >>ud ad<<, væk fra det sted hvor stenen eller fingerspidsen rammer vandoverfladen.

\begin{figure}
\centering
\includegraphics[width=\linewidth]{./grafik/bolger/water-surface-waves.jpg}
\caption{Ringbølger i en vandoverflade.\citep{surface-water-wave-fingers}}
\label{fig:water-surface-waves}
\end{figure}

På samme måde ses det tydeligt at en bølge på en snor bevæger sig væk fra den ende hvor man ryster snoren.
%Først bevæger bølgen sig fremad, når den rammer snorens ende, reflekteres bølgen og bevæger sig tilbage.  

\begin{figure}
\centering
\href{https://phet.colorado.edu/sims/html/wave-on-a-string/latest/wave-on-a-string_en.html}{\includegraphics[width=\linewidth]{./grafik/bolger/snorbolge2.png}}
\caption{Når snoren rystes i venstre side, dannes der en bølge som løber hen ad snoren, en snorbølge.\citep{waves-on-string-animation}}
\label{fig:water-surface-waves}
\end{figure}


Bølger der bevæger sig fremad på denne måde, kaldes for rejsende bølger\index{rejsende bølger}.
Men bemærk at det kun er selve forstyrrelsen der bevæger sig fremad, de enkelte vandmolekyler (eller snormolekyler) bevæger sig kun op og ned.
Og når vandoverfladen (eller snoren) er faldet til ro igen, et stykke tid efter at bølgen er passeret, så vil alle molekyler befinde sig samme sted som de gjorde før bølgen passerede. En bølge er kendetegnet ved at der sker en transport af energi eller information, uden at der sker en transport af stof. 

\href{https://dl.dropboxusercontent.com/u/5481734/opentextbooks/animationer/travelling-and-standing-waves.mp4}{Denne animation} \marginnote{\url{https://dl.dropboxusercontent.com/u/5481734/opentextbooks/animationer/travelling-and-standing-waves.mp4}} viser to rejsende bølger, den ene (blå) bevæger sig mod højre og den anden (rød) bevæger sig mod venstre. Desuden er det også vist hvordan den resulterende bølge (sort) vil se ud hvis to sådanne rejsende bølger interfererer. 



\begin{formelboks}
En sinusbølge der bevæger sig mod højre:


\begin{equation} 
\label{eq:traveling-wave-right}
h(x,t) = A\cdot \sin{\left( \frac{2\pi}{\lambda} \cdot x - \frac{2\pi}{T} \cdot t\right)}
\end{equation}


En sinusbølge der bevæger sig mod venstre:

\begin{equation}
\label{eq:traveling-wave-left}
v(x,t) = A\cdot \sin{\left( \frac{2\pi}{\lambda} \cdot x + \frac{2\pi}{T} \cdot t\right)}
\end{equation}

\end{formelboks}


%\subsection{Cirkelbølger og kuglebølger}
%Vi har tidligere set at hvis man smider en sten i vandet, så udbreder der sig en bølge som ringe/cirkler i vandoverfladen.
%
%
%
%\subsection{Plane bølger}
%plan bølge er en approximation 
%


\section{Stående bølger}
\label{sec:staande-bolger}
Lad os tage en sinusbølge der rejser mod højre, og en sinusbølge der rejser mod venstre. Hvis vi lader disse to bølge interferere får vi en resulterende bølge som opfører sig lidt specielt.

\href{http://tube.geogebra.org/m/RHVIzYMp}{Denne animation} \marginnote{\url{http://tube.geogebra.org/m/RHVIzYMp}} viser funktionen \(h(x,t)\) farvet blå, funktionen \(v(x,t)\) farvet rød, samt den resulterende bølge \(v(x,t)+h(x,t)\) farvet sort. Af animationen ses det, at når to identiske (men modsatrettede) rejsende bølger interfererer, så bliver resultatet en resulterende bølge der ser ud til at svinge op og ned uden at bevæge sig, og uden at ændre form. En sådan bølge kaldes derfor for en stående bølge.


\begin{formelboks}
En stående bølge kan beskrives ved følgende formel:

\begin{equation}
\label{eq:standing-wave}
S(x,t) = 2A \cdot \sin{\left(\frac{2\pi}{\lambda} \cdot x \right)} \cdot \cos{\left(\frac{2\pi}{T}\cdot t \right)}
\end{equation}

Denne formel fremstiller, i modsætning til \eqref{eq:sinusbolge}, ikke en rejsende bølge, men derimod en bølge der har svingningstiden \(T\) og en amplitude der afhænger af positionen \(x\).
\end{formelboks}

Ovenstående formel kan udledes ved at lave matematiske omskrivninger af \(h(x,t)+v(x,t)\). Dvs ved at se på interferensen mellem to identiske, og modsatrettede, rejsende bølger. \marginnote{ 
\begin{tikzpicture}
 \draw [beaublue, line width = 4] (0,0) -- (0.25,0); 
\end{tikzpicture}
\opgref{opg:staaende-bolge}
}



\section{Komplekse bølger}
Vi har i dette afsnit beskæftiget os med pæne sinusbølger. Men hvis man for eksempel ser på en optagelse af en lydbølge fra et instrument (se figur \vref{fig:}), så opdager man at bølgen ser meget kompleks ud, og den ligner i hvert fald ikke en pæn sinusfunktion. 

\begin{figure}
\centering
\begin{tikzpicture}

\draw (0,0) -- (17,0) -- (17,4) -- (0,4) -- cycle;

\end{tikzpicture}
\caption{Optagelse af lydbølge fra mundharmonika (vha. Loggerpro)}
\label{fig:kompliceret-lydbolge}
\end{figure}

Så kan vi overhovedet bruge vores matematiske teori om sinusbølger til at beskrive virkelige fysiske fænomener, som fx en lydbølge der bevæger sig gennem rummet?  

Det viser sig faktisk at enhver periodisk funktion kan skrives som en sum af sinus- og cosinusfunktioner. Dette er en meget vigtigt erkendelse indenfor matematik, fysik og ingeniørvidenskaberne. Det betyder nemlig at enhver (periodisk) bølge uanset hvor kompliceret den er, kan beskrives vha. de forholdvis simple sinusfunktioner som vi har beskæftiget os med i dette afsnit.

Figur \vref{fig:} viser netop et eksempel på hvordan en mere kompliceret lydbølge kan opbygges af simple sinusfunktioner med forskellige bølgelængder og amplituder. 
	
 


