\chapter{Lydbølger}
\label{chap:lyd}
\backgroundsetup{
position=current page.north east,
vshift=-35pt,
hshift=-200pt,
angle=0,
scale=0.4,	
opacity=0.2,
contents={\includegraphics{./grafik/forside/my-by-sa.png}}
}
\BgThispage

\begin{figure*}
\centering
\includegraphics[width=\linewidth]{./grafik/bolger/soundwaves-flickr-cropped.png}
\caption{Kunstnerisk illustration af lydbølger. \citep{kunst-lydbolge}}
\label{fig:lydbolger-kunstnerisk}
\end{figure*}

\newthought{Lydbølger} Det vi normalt kalder lyd er længdebølger der udbreder sig gennem den luft der omgiver os. Lydbølger transporterer energi. Når denne energi rammer ørets trommehinde sættes trommehinden i svingninger og disse svingninger omsættes, via ørets komplicerede indretning, til elektriske nerveimpulser der fortolkes som lyd af hjernen. Når en lydbølge udbreder sig gennem luften fra en person der taler, til en person der lytter, så er det ikke sådan at luftmolekylerne bevæger sig fra den ene persons mund til den anden persons øre. En bølge transporterer ikke stof, men kun energi. For mekaniske bølger gælder det generelt at en bølges energi afhænger af frekvensen og amplituden\footnote{For elektromagnetiske bølger som lys afhænger energien lidt overraskende, kun af frekvensen: \( E_{foton}=h \cdot f \).}. Forestil dig, at du skal sende en snorbølge afsted langs et reb ved at svinge rebets ene ende op og ned. Så virker det rimeligt nok, at jo større amplitude bølgen skal have, jo mere energi skal du bruge. Det virker også rimeligt at jo hurtigere du svinger rebet op og ned, jo mere energi skal du bruge.   


Men hvad er det egentligt der svinger, når en længdebølge som fx en lydbølge bevæger sig gennem et medie? Det er mediets (lokale) densitet\footnote{Densitet er blot et andet ord for massefylde. Massefylden er defineret som \[ \rho = \frac{m}{V} \]
\(m\) er genstandens masse, \(V\) er genstandens rumfang og \(\rho\) er symbolet for massefylden.} som >>svinger<<, molekylerne et givet sted i mediet bliver skiftevis trukket fra hinanden og presset sammen mens lydbølgen passerer. Hvis lydbølgen har en stor amplitude vil densiteten (lokalt) ændre sig meget i forhold til mediets >>uforstyrrede<< densitet. Vi kan med det blotte øje ikke se, at molekyltætheden ændrer sig, men på figur \vref{marginfig:soundmedium-density} er det forsøgt illustreret hvordan luftens densitet ændrer sig pga. lyden fra en stemmegaffel. 


\begin{marginfigure}
\centering
\includegraphics[width=\linewidth]{./grafik/bolger/soundwave.png}
\caption{Når en lydbølge udbreder sig gennem luft vil luftens densitet svinge omkring densiteten af den uforstyrrede luft. \citep{soundmedium-density}}
\label{marginfig:soundmedium-density}
\end{marginfigure}



Luftens tryk afhænger blandt andet af luftens densitet. Det betyder at luftens tryk ændres når en lydbølge passerer. Når luftens densitet er høj vil lydtrykket blive lidt højere end trykket i den uforstyrrede luft. Ligeså vil luftens tryk være lidt lavere når luftens densitet er lav. Jo større variation i lydtrykket jo kraftigere opleves lyden.



\section{Øret}
Øret er et meget kompliceret sanseorgan som vi her kun vil beskrive overfladisk. Figur \vref{fig:anatomi-ore} viser en skitse af ørets anatomi.

\begin{figure}[htpb]
\centering
\includegraphics[width=\linewidth]{./grafik/bolger/Anatomy_Human_Ear.png}
%\setfloatalignment{b}% forces caption to be aligned with the bottom of the float contents
\caption{Ørets anatomi. \citep{anatomi-ore}}
\label{fig:anatomi-ore}
\end{figure}

En ca. \(30\) \si{mm} lang øregang fører fra det ydre øre og ind til trommehinden. Når lydbølger rammer trommehinden sættes den i svingninger, og trommehindens bevægelse overføres via tre små knogler hammeren, ambolten og stigbøjlen, til en anden hinde kaldet det ovale vindue. Det ovale vindue kan ikke ses på figur \vref{fig:anatomi-ore} fordi det befinder sig bagved stigbøjlen. De tre små knogler, hammeren, ambolten og stigbøjlen, befinder sig i et luftfyldt rum som står i forbindelses med atmosfæren via det eustatiske rør. Rummet bag det ovale vindue er væskefyldt. Væsken overfører bevægelsen til en række meget følsomme >>hår<< som findes i sneglen. Disse hår er vitale for hørelsen, hvis de på den ene eller den anden måde ødelægges, fx ved at de udsættes for kraftig støj i længere tid, giver det varigt høretab. Buegangene er væskefyldte rør der udgør vores balanceorgan (vores indbyggede gyroskop).


Det menneskelige øre er mest følsomt i frekvensområdet fra \(200\) \si{Hz} til \(4000\) \si{Hz}, hvilket også er frekvensområdet for almindelig tale. Øret kan dog opfatte lyd med frekvenser i et langt større område. Unge mennesker kan høre lyde med frekvenser helt op i nærheden af \(20000\) \si{Hz}, denne evne aftager dog stærkt med alderen. Frekvensområdet for en god højttaler er fra ca. \(25\) Hz til \(20000\) \si{Hz}.


Da ørets følsomhed overfor toner med forskellige frekvenser varierer, betyder det, at fx en tone med en frekvens på \(1000\) \si{Hz}, der afspilles med et lydstyrke på \(40\) \si{dB} af øret vil opfattes som meget kraftigere end en tone på \(100\) \si{Hz} der afspilles med samme lydstyrke\footnote{Læs mere om lydstyrke og dB \vref{sec:lydstyrke}.}. 

\subsection{Hørestyrke}
For at tage højde for det forhold, at ørets følsomhed er frekvensafhængig indføres begrebet \emph{hørestyrke}\index{hørestyrke}. Hørestyrken måles i enheden fon (på engelsk, phon). Figur \vref{fig:horestyrke} er baseret på målinger af mange menneskers hørelse, og kan lære os om ørets følsomhed. På x-aksen er afsat frekvensen i \si{Hz}, og på y-aksen er afsat lydstyken i \si{dB}. Kurverne i de forskellige farver forbinder punkter der har samme hørestyrke. Følges den lysegrønne \(40\) \si{fon} kurve ses at en \(1000\) \si{Hz} tone med et lydstyrke på \(40\) \si{dB} har samme hørestyrke som en tone på \(30\) \si{Hz} ved \(90\) \si{dB}. På \href{http://newt.phys.unsw.edu.au/jw/hearing.html}{dette link} \marginnote{\url{http://newt.phys.unsw.edu.au/jw/hearing.html}} kan du teste din egen hørelse, og opdage hvordan ørets følsomhed afhænger af lydens frekvens. Som der også tydeligt advares om på hjemmesiden, skal du passe meget på med at skrue for højt op - læs anvisningerne inden du prøver høretesten!       


\begin{figure}[htpb]
\centering
\includegraphics[width=\linewidth]{./grafik/bolger/horestyrke.png}
%\setfloatalignment{b}% forces caption to be aligned with the bottom of the float contents
\caption{Ørets følsomhed afhænger af lydens frekvens. De farvede fuldt optrukne linjer angiver samme hørestyrke. Den stiplede linje angiver høregrænsen for det menneskelige øre. \citep{horestyrke}}
\label{fig:horestyrke}
\end{figure}


\section{Lydens hastighed}
Vi skal i dette afsnit se nærmere på lydens hastighed. De fleste tænker nok umiddelbart på lyd som noget der bevæger sig gennem luften, men hvis du har en støjende nabo, er du også bekendt med at lyd kan bevæge sig gennem materialer som mursten og rockwool. Lydens hastighed afhænger meget kraftigt af hvilket materiale lyden bevæger sig igennem. 
%Tabel \vref{margintab:lydhast-gasser, tab:lydhast-vaeske, tab:lydhast-solid} giver en oversigt over lydens hastighed i forskellige udvalgte materialer.
For alle materialer afhænger lydens hastighed også af temperaturen. For atmosfærisk luft afhænger hastigheden desuden af mængden af vanddamp i luften, og for vand har saltindholdet betydning.

\subsection{Hastighed}
Generelt er hastighed en fysisk størrelse, der siger noget om hvor hurtigt en genstand (eller et punkt på en bølge) ændrer sin position. 
I fysik defineres hastighed som tilbagelagt afstand per tid. Udtrykt ved en formel skrives:

\[ v = \frac{\Delta{s}}{\Delta{t}} = \frac{s_{slut}-s_{start}}{t_{slut}-t_{start}} \]

Hvor \(v\) er hastigheden, \(\Delta{s}\) er den tilbagelagte afstand og \(\Delta{t}\) er den tid det tog at tilbagelægge afstanden.



\subsection{Sonar}
Hvis man kender lydens hastighed i vand kan man bruge lyd til at bestemme afstande til genstande under vand. Når en ubåd skal navigere under vandet og undgå at støde ind i ting, benytter ubåden sig af sin sonar (SOund NAvigation and Ranging).
Princippet bag en sonar er simpelt. En ubåd udsender et lydsignal, hvis lyden rammer en genstand fx en hval, så vil noget at lyden reflekteres tilbage til ubåden. Ved at måle hvor lang tid der er gået mellem udsendelse af lyden og modtagelse af lydens reflektion (ekkoet), kan man bestemme afstanden til hvalen.

En radar (RAdio Detection And Ranging) bygger på samme princip. Forskellen er blot at en radar benytter elektromagnetiske bølger (radiobølger) i stedet for lydbølger. 


Flagermus bruger også denne metode til at navigere, så flagermus ved altså åbenbart hvad lydens hastighed er i atmosfærisk luft. Lydens hastighed afhænger som nævnt af flere ting bl.a. temperatur,  og hvis flagermus skal kunne navigere korrekt, skal de kunne tage højde for dette.  


\subsection{Lydens hastighed i gas}
\begin{margintable}
\centering

\resizebox{\textwidth}{!}{
\begin{tabular}{cc} 

\toprule
Gas & lydhastighed [m/s] \\ \midrule
Atmosfærisk luft ( \(0 \si{^\circ C}\) ) & 331 \\
Atmosfærisk luft ( \(20 \si{^\circ C}\) ) &  343 \\
CO2 ( \(51 \si{^\circ C}\) ) & 280 \\
Helium ( \(0 \si{^\circ C}\) ) & 972 \\
Hydrogen ( \(0 \si{^\circ C}\) ) & 1290 \\
Oxygen ( \(30 \si{^\circ C}\)) & 332 \\
Vanddamp ( \(100 \si{^\circ C}\) ) & 478 \\
Svovl-hexafluoride ( \(11 \si{^\circ C}\) ) & 133 \\ \bottomrule
\end{tabular}
}
\vfill
\caption{Lydens hastigheder i forskellige gasser. \newline \url{http://www.engineeringtoolbox.com/speed-sound-gases-d_1160.html}}
\label{margintab:lydhast-gasser}
\end{margintable}


Ved komplicerede teoretiske udledninger kan man finde frem til følgende formel for lydens hastighed gennem atmosfærisk luft

\begin{formelboks}
Lydens hastighed i luft, som funktion af temperaturen. 

\begin{equation}
 v_{luft} = \sqrt{ \sigma \cdot T} \quad \text{Hvor} \quad \sigma = 402.56 \frac{m^2}{s^2 \cdot K}
\label{eq:lydhast-luft} 
\end{equation}

Her er \(\sigma\) en materialekonstant som er specifik for atmosfærisk luft, og \(T\) er temperaturen i kelvin. 

\end{formelboks}




\subsection{Lydens hastighed i væske}
På samme måde som for gasser, kan man ved komplicerede teoretiske udledninger komme frem til en formel for lydens hastighed i væsker.  

\begin{margintable}
\centering

\resizebox{\textwidth}{!}{
\begin{tabular}{cc} 

\toprule
Stof & Lydhastighed [m/s] \\ \midrule
Flydende helium (4 K) & 211 \\
Kviksølv (20 \si{^\circ C})& 1451   \\
Vand (0 \si{^\circ C}) & 1402 \\
Vand (20 \si{^\circ C}) & 1482 \\ \bottomrule
\end{tabular}
}
\vfill
\caption{Lydens hastighed i forskellige væsker. Bemærk at temperaturen også har indflydelse på lydens hastighed.}
\label{tab:lydhast-vaeske}
\end{margintable}

\begin{formelboks}
Hastigheden af lydbølger i væske kan beregnes ved hjælp af nedenstående formel.

\begin{equation}
 v_{\textup{væske}} = \sqrt{ \frac{B}{\rho} }
\label{eq:lydhast-solid} 
\end{equation}

\( \rho \) er væskens densitet og \( B \) er en materialeparamter som afhænger af hvor svært det er at presse væsken sammen (på engelsk kaldes B for stoffets \textit{bulk modulus}).
\end{formelboks}


Tabel \vref{tab:lydhast-vaeske} angiver lydhastigheden i forskellige væsker, og figur \vref{fig:lydhast-vaeske} viser hvordan lydens hastighed i vand afhænger af vandets temperatur. %Noget tilsvarende gælder som sagt for andre væsker.

\begin{figure}[htpb]
\centering
\includegraphics[width=\linewidth]{./grafik/bolger/lydhastighed-vand.png}
%\setfloatalignment{b}% forces caption to be aligned with the bottom of the float contents
\caption{Lydhastigheden i vand afhænger af temperaturen. \citep{lydhast-vand}}
\label{fig:lydhast-vaeske}
\end{figure}






\subsection{Lydens hastighed i fast stof}
I fast stof bliver tingene endnu mere kompliceret end for gasser og væsker. I fast stof kan bølger både udbrede sig som længde- og tværbølger. Længdebølgerne (dvs. lydbølgerne) vil typisk bevæge sig med størst hastighed. Tabel \vref{tab:lydhast-solid} angiver hastigheden for lydbølger i forskellige materialer. 

\begin{formelboks}
Hastigheden af længdebølger(lydbølger) i fast stof kan beregnes vha. nedenstående formel.

\begin{equation}
 v_{stof} = \sqrt{ \frac{Y}{\rho} }
\label{eq:lydhast-solid} 
\end{equation}

\( \rho \) er stoffets densitet og \( Y \) er en materialeparamter som afhænger af hvor elastisk stoffet er. (Y kaldes for stoffets elasticitetsmodul). 
\end{formelboks}

\begin{margintable}
\centering
\resizebox{\textwidth}{!}{
\begin{tabular}{cc} 

\toprule
Stof & Lydhastighed [m/s] \\ \midrule
Aluminium & 6420 \\
Stål & 5941 \\
Bly & 1960 \\ \bottomrule
\end{tabular}
}
\vfill
\caption{Lydens hastighed i forskellige faste stoffer.}
\label{tab:lydhast-solid}
\end{margintable}


Jorden under vores fødder er et fast stof, hvor der både kan udbrede sig længdebølger og tværbølger, og ved et jordskælv vil der fra jordskælvets epicenter, udbrede sig energi i form af bølger. Længdebølgerne bevæger sig hurtigt gennem jorden og vil derfor blive registreret først, af seismograferne. Denne første bølge kaldes af seismologer for primærbølgen. Lidt efter at primærbølgen har ramt kommer sekundærbølgen. Sekundærbølgen er en tværbølge og har derfor bevæget sig langsommere gennem jorden. Men tilgengæld gør tværbølgens bevægelse oftest mere skade på fx byer og veje, end længdebølgerne. En tværbølge vil få jorden til at bevæge sig fra side til side eller op og ned. En længdebølge derimod vil blot skiftevis presse jorden lidt sammen og trække den fra hinanden.   



\section{Lydstyrke}
\label{sec:lydstyrke}
En lydbølge er som nævnt en forstyrrelse der udbreder sig gennem luften og transporterer (lyd)energi fra et sted til et andet. Et mål for hvor meget energi bølgen transporterer får man ved at indføre intensiteten. Intensiteten defineres som den energi der hvert sekund transporteres gennem en \(1 \si{m^{2}}\) stor flade, der står vinkelret på bølgens udbredelsesretning. Udtrykt ved en formel defineres intensiteten på følgende måde.


\begin{marginfigure}
\centering
\includegraphics[width=\linewidth]{./grafik/bolger/sound-intensity.png}
\caption{En lydkilde udsender lyd i alle retninger. En måling af lydintensiteten afhænger af hvor meget lydenergi der passerer et givet areal pr. sekund.}
\label{fig:soundintensity-margin}
\end{marginfigure}



\begin{formelboks}
Definition af lydintensiteten \(I\).

\begin{equation}
I = \frac{\Delta{E}}{\Delta{t} \cdot A}
\end{equation}

\(\Delta{}E\) angiver den mængde lydenergi der i tidsrummet \(\Delta{}t\) transporteres (vinkelret) gennem en flade med arealet \(A\). 
SI-enheden for lydintensitet er joule pr. sekund pr. kvadratmeter, hvilket skrives som \( \si{ \tfrac{J}{s \cdot m^{2}} } \).

\end{formelboks}


Menneskets øre kan opfatte lyde med intensiteter helt ned til \( 10^{-12} \si{ \tfrac{J}{s \cdot m^{2}}} \). Denne intensitet kaldes også for høregrænsen og betegnes med symbolet \( I_{0} \). For intensiteter højere end \(1 \si{ \tfrac{J}{s \cdot m^{2}}} \) udløser lyden smerte, og denne intensitet kaldes derfor for smertetærsklen. 


Vores lydopfattelse er ikke ligefrem proportional med intensiteten, hvis lydenergien (og dermed intensiteten) fordobles, så vil lydenstyrken ikke opfattes som dobbelt så stor. Dette fænomen kender du sikkert fra din hverdag. Hvis to personer synger duet kan man høre at lydstyrken bliver større i forhold til hvis der kun er én sanger, men lydstyrken bliver ikke dobbelt så stor. 
Ved forsøg har det vist sig, at hvis en lydgiver udsender lyd med en intensitet, der er 10 gange større end intensiteten fra en anden lydgiver, så opfattes lyden ikke 10 gange så kraftig, men kun ca. dobbelt så kraftig som den svagere lydkilde.

Hvis du lytter til førnævnte duet kan du sikkert godt høre at lydstyrken bliver større når begge sangere synger, men hvis du lytter til et kor kan du sikkert ikke høre på lydstyrken om der er 29 eller 30 sangere der synger. Heraf kan man konkludere at ørets evne til at registrere en ændring i lydstyrken ikke (kun) afhænger af den absolutte ændring i lydintensiteten. Det viser sig at ørets evne til at registrere lydændringer afhænger af den procentvise ændring af lydintensiteten.
Mere præcist viser det sig, at logaritmefunktionen er en god matematisk model for sammenhængen mellem lydintensiteten og ørets opfattelse af lydstyrken. Dette giver anledning til følgende definition af lydstyrken. 

\begin{formelboks}
Definition af lydstyrke.   

\begin{equation}
L = 10 \cdot \log{\left( \frac{I}{I_{0}} \right)}
\end{equation}

Enheden for lydstyrken \(L\) er decibel hvilket forkortes som dB. \(I_{0} = 10^{-12} \si{ \tfrac{J}{s \cdot m^{2}}}\) er den mindste intensitet, der kan registreres af et menneske med normal hørelse, og kaldes for høregrænsen. \( I \) er lydens intensitet.
\end{formelboks}


Typiske lydintensiteter og lydstyrker fremgår af tabel \vref{tab:lydstyrker}.


\begin{table}[htpb]
\centering
\begin{tabular}{ccc} 

\toprule
 & Intensitet \( \si{\frac{J}{s \cdot m^{2}}} \) & Lydstyrke \( \si{dB} \) \\ \midrule
Høregrænse & \( 10^{-12} \) & 0\\
Raslen i løvet & \( 10^{-11} \) & 10 \\ 
Hvisken & \( 10^{-10} \)& 20\\
Samtale & \( 10^{-6} \) & 60\\
Trafikeret vej & \( 10^{-5} \) & 70\\
Støvsuger & \( 10^{-4} \)& 80 \\
Symfoniorkester & \( 10^{-3} \) & 90\\
Ipod (maximum) & \( 10^{-2} \) & 100\\
Rockkoncert & \( 10^{-1} \) & 110\\
Smertegrænse & \( 10^{1} \) & 130\\
Jetfly ved start & \( 10^{2} \) & 140\\
Perforering af trommehinden & \( 10^{4} \) & 160\\ \bottomrule

\end{tabular}
\caption{Typiske lydintensiteter og tilhørende lydstyrker. }
\label{tab:lydstyrker}
\end{table}






