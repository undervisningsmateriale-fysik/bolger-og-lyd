# -*- coding: utf-8 -*-
"""
Created on Tue Jul  7 18:53:45 2015

@author: msm
"""

# import packages to ensure python3 compatibility
from __future__ import print_function
from __future__ import division
#from __future__ import unicode_literals

# import packages
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
#from matplotlib import cm
from matplotlib import animation


# make graph
fig = plt.figure()    
ax = plt.subplot(111, projection='3d' )
#ax.plot_wireframe(x,t,f)

line, = ax.plot3D([], [], [])
#cset = ax.contour(x, y, f, zdir='x', offset=6, cmap=cm.coolwarm)
#cset = ax.contour(x, y, f, zdir='y', offset=-6, cmap=cm.coolwarm)


# layout
ax.set_xlabel('Sted [meter]')
ax.set_ylabel('Sted [meter]')
ax.set_zlabel('')
#ax.grid(False)

ax.set_zticks([])

# set "camera" view
ax.elev = 80 # elevation
ax.azim = 70 # azimuth angle
ax.dist = 8 # distance

def init():
    line.set_data([],[])
    return line,
    
    
def animate(i):
    # choose x- and t-values for graph
    x = np.arange(-10, 10, 0.4)
    y = np.arange(-10,10,0.4)

    # make x,y grid
    x,y = np.meshgrid(x,y)
    
    # constants
    lamb = 2
    #T = 2

    # calculate wave
    f = np.exp(-0.2*np.sqrt(x**2+y**2))*np.sin(2*np.pi/lamb*np.sqrt(x**2+y**2)+ 0.25*i)
    line.set_data(x, y ,f)
    return line,

anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=20, interval=20, blit=True)

anim.save('basic_animation.mp4', writer='mencoder', fps=30)

plt.show()