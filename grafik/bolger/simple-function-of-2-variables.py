# -*- coding: utf-8 -*-
"""
Created on Tue Jul 14 21:57:53 2015

@author: msm
"""

# import packages to ensure python3 compatibility
from __future__ import print_function
from __future__ import division
#from __future__ import unicode_literals

# import packages
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm

# make graph
fig = plt.figure()    
#ax = fig.add_axes([0, 0, 0.85, 0.85], projection='3d')
ax = plt.subplot(111, projection='3d' )

x = np.arange(0,4,0.05)
y = np.arange(0,4,0.05)
x,y = np.meshgrid(x,y)

ax.plot_surface(x,y,2*x+y)

ax.set_xlabel('x', fontsize='22')
ax.set_ylabel('y', fontsize='22')
ax.set_zlabel('f(x,y)', fontsize='22')
#ax.grid(False)
#ax.set_zticks([])

# prepare the axes limits
ax.set_xlim((0,4))
ax.set_ylim((0,4))
ax.set_zlim((0, 13))

# set contours
#cset = ax.contour(x, y, 2*x+y, zdir='x', offset=-5, cmap=cm.coolwarm)
#cset = ax.contour(x, y, 2*x+y, zdir='y', offset=5, cmap=cm.coolwarm)

# set "camera" view
#ax.elev = 20 # elevation
#ax.azim = -90 # azimuth angle
#ax.dist = 8 # distance

plt.show()